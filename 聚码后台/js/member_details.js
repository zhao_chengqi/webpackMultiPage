
 $(window).resize(function() {
    // paintingecharts ()
  });
  paintingecharts ()
function paintingecharts (){
    var dom = document.getElementById("echarts");
    var myChart = echarts.init(dom);
    var app = {};
    option = null;
    app.title = '坐标轴刻度与标签对齐';
   
    option = {
        color: ['#3398DB'],
        tooltip: {
            trigger: 'axis',
            axisPointer: { // 坐标轴指示器，坐标轴触发有效
                type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [{
            type: 'category',
            data: ['5月1号', '5月2号', '5月1号', '5月1号', '5月1号', '5月1号', '5月1号','5月1号', '5月1号', '5月1号', '5月1号', '5月1号', '5月1号', '5月1号','5月1号', '5月1号', '5月1号', '5月1号', '5月1号',],
            axisTick: {
                alignWithLabel: true
            }
        }],
        yAxis: [{
            type: 'value'
        }],
        series: [{
            name: '直接访问',
            type: 'bar',
            barWidth: '60%',
            data: [1000, 5, 2000, 334, 390, 3300, 20,10, 52, 900, 334, 390, 330, 220,200, 334, 390, 330, 220]
        }]
    };;
    if (option && typeof option === "object") {
        myChart.setOption(option, true);
    }
}

 $(".classification>.source>.but").click(function () {
     $(".classification>.source>.but>.list").height(0);
     $(".classification>.source>.but").css("background", "#fff");
     $(this).css("border-radius", "5px");
     if ($(this).children(".list").length) {
         $(this).children(".list").height($(this).children(".list").children(".item").length *
             33 + 'px');
         $(this).css("background", "#EFEFEF");
         $(this).css("border-radius", "5px 5px 0 0");
     }
 })

 $(".classification>.source>.but").mouseout(function (event) {
     var tar = event.target || event.srcElement; //鼠标离开的元素  
     var totar = event.relatedTarget || event.toElement; //鼠标指向的元素   
     if (totar != $(this)[0] && !$(totar).parent().is(".list") && $(
             ".classification>.source>.but>.list").height() != '33') {
         $(".classification>.source>.but>.list").height(0);
         $(this).css("background", "#fff");
         $(this).css("border-radius", "5px");
     }
 });
 $(".classification>.source>.but>.list>.item").click(function () {
     $(this).parent().parent(".but").children("span").text($(this).text());
 })
 //日期时间范围
 laydate.render({
     elem: '#test10',
     // type: 'datetime',
     range: true
 });
 $("#nav>div").click(function () {
     var $index = $(this).index();
     $("#nav>div").removeClass('active').eq($index).addClass('active');
     $("#navcontent>div").removeClass('active').eq($index).addClass('active');
 })
 //会员基础和其他信息 切换
 $("#infomenu>div").click(function(){
    $("#infomenu>div").removeClass('active'); 
    $(this).addClass("active");
    $("#information>.content").removeClass('active'); 
    $("#information>.content").eq($(this).index()).addClass("active");
 })