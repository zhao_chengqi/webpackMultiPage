/**
 * body点击事件需要隐藏的节点
 * @param {节点} dom 
 * @param {节点唯一标识} Udf 
 */
var bodyClick = (function () {
    var bhide = [];
    return function (dom, Udf) {
        var state = false;
        for (var item of bhide)
            if (item.Udf == Udf)
                state = true;
        if (!state)
            bhide.push({
                Udf: Udf,
                dom: dom
            });
        $("body").unbind("click");
        $("body").click(function () {
            for (var item of bhide) {
                item.dom.hide();
            }
        })
    }
})()
/**
 * 返回上一页
 */
function goBack() {
    window.history.go(-1);
}

/**
 *关闭菜单
 *
 */
function closeMenu() {
    $("#iflogo").addClass("collect");
    $("#ifmenu").addClass("collect");
}

function openMenu() {
    $("#iflogo").removeClass("collect");
    $("#ifmenu").removeClass("collect");
}


//选择 一页显示多少
$("#page").click(function (ev) {
    var oEvent = ev || event;
    oEvent.cancelBubble = true;
    oEvent.stopPropagation();
    bodyClick($("#pagenumber"), "#pagenumber")
    if ($("#pagenumber").css('display') == 'none')
        $("#pagenumber").show()
    else
        $("#pagenumber").hide()
})
$("#pagenumber>div").click(function (ev) {
    $("#page>span").text($(this).text());
})


/**
 * 数组去重
 */
function distinct(array) {
    var arr = array,
        result = [];
    arr.forEach(function (v, i, arr) { //这里利用map，filter方法也可以实现
        var bool = arr.indexOf(v, i + 1); //从传入参数的下一个索引值开始寻找是否存在重复
        if (bool === -1) {
            result.push(v);
        }
    }) 
    return result;
};