const fs = require('fs');
const path = require("path");
/**
 * 读取路径信息
 * @param {string} path 路径
 */
function getStat(path) {
    return new Promise((resolve, reject) => {
        fs.stat(path, (err, stats) => {
            if (err) {
                resolve(false);
            } else {
                resolve(stats);
            }
        })
    })
}

/**
 * 创建路径
 * @param {string} dir 路径
 */
function mkdir(dir) {
    return new Promise((resolve, reject) => {
        fs.mkdir(dir, err => {
            if (err) {
                resolve(false);
            } else {
                resolve(true);
            }
        })
    })
}

/**
 * 路径是否存在，不存在则创建
 * @param {string} dir 路径
 */
async function dirExists(dir) {
    let isExists = await getStat(dir);
    //如果该路径且不是文件，返回true
    if (isExists && isExists.isDirectory()) {
        return true;
    } else if (isExists) { //如果该路径存在但是文件，返回false
        return false;
    }
    //如果该路径不存在
    let tempDir = path.parse(dir).dir; //拿到上级路径
    //递归判断，如果上级目录也不存在，则会代码会在此处继续循环执行，直到目录存在
    let status = await dirExists(tempDir);
    let mkdirStatus;
    if (status) {
        mkdirStatus = await mkdir(dir);
    }
    return mkdirStatus;
}


//复制文件的原理 先读取文件，成功读取文件之后，再写文件，写文件的第一个参数是写的文件的文件名
// fs.readFile("process.js","utf-8",function(err){
//     if(!err){
//         fs.writeFile("process2.js","utf-8",function(err){
//             if(!err){
//                 console.log("复制文件成功");
//             }else{
//                 console.log("复制文件失败"+err.message);
//             }
//         })
//     }
// })


//默认生成的文件
const dirArr = [{
    dir: 'css',
    childrenFile: '/index.css'
}, {
    dir: 'entry',
    childrenFile: '/index.js',
    childrenFileWrite: 'import "../less/index.less";'
}, {
    dir: 'images'
}, {
    dir: 'js',
    childrenFile: '/index.js'
}, {
    dir: 'less',
    childrenFile: '/index.less',
    childrenFileWrite: '@import "./global.less";'
}, {
    dir: 'svg'
}];
dirExists(path.resolve(__dirname, "../src/")); 


//生成默认html内容
fs.readFile(__dirname + '/resources/index.html', {
    flag: 'r+',
    encoding: 'utf8'
}, function (err, data) {
    if (err) {
        console.error(err);
        return;
    }
    fs.writeFile(path.resolve(__dirname, "../src/index.html"), data, function (err) {
        if (!err) {
            console.log("html文件生成成功");
        } else {
            console.log("html文件生成失败" + err.message);
        }
    });
});


//创建及设置默认内容
dirArr.forEach(element => {
    dirExists(path.resolve(__dirname, "../src/", element.dir));
    element.childrenFile && fs.writeFile(path.resolve(__dirname, "../src/", element.dir) + element.childrenFile, element.childrenFileWrite ? element.childrenFileWrite : "", function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
    element.childrenFileWrite
});


//生成默认less样式
fs.readFile(__dirname + '/resources/global.less', {
    flag: 'r+',
    encoding: 'utf8'
}, function (err, data) {
    if (err) {
        console.error(err);
        return;
    }
    fs.writeFile(path.resolve(__dirname, "../src/less/global.less"), data, function (err) {
        if (!err) {
            console.log("less文件生成成功");
        } else {
            console.log("less文件生成失败" + err.message);
        }
    });
});


//生成默认rem js内容
fs.readFile(__dirname + '/resources/SetSize.js', {
    flag: 'r+',
    encoding: 'utf8'
}, function (err, data) {
    if (err) {
        console.error(err);
        return;
    }
    fs.writeFile(path.resolve(__dirname, "../src/js/SetSize.js"), data, function (err) {
        if (!err) {
            console.log("rem设置成功");
        } else {
            console.log("rem设置失败" + err.message);
        }
    });
});
