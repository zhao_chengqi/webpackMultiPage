const merge = require('webpack-merge');
const webpaskBase=require('./webpack.base.js');
module.exports=merge(webpaskBase,{ 
    mode: 'production',//production
    //出口文件的配置项
    output: { 
        publicPath: "./"//publicPath：主要作用就是处理静态文件路径的。
    },
})