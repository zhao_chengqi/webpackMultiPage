const path = require("path");
const fs = require("fs");
//同步检查html模板
const glob = require('glob');
//js压缩
const uglify = require('uglifyjs-webpack-plugin');
//html
const htmlPlugin = require('html-webpack-plugin');
//css分离 
const extractTextPlugin = require('extract-text-webpack-plugin');
//使用PurifyCSS可以大大减少CSS冗余
const PurifyCSSPlugin = require("purifycss-webpack");

//webpack.config.js
//删除打包后文件的内容
const CleanWebpackPlugin = require('clean-webpack-plugin');

const CopyWebpackPlugin = require('copy-webpack-plugin');

//设置为你的目标文件夹地址
var buildDir = '../dist/';
//入口文件
var pageEntry = {}
var entryDir = fs.readdirSync(path.resolve(__dirname, "../src/entry"));
//得到入口文件
entryDir.forEach((item, index) => {
    pageEntry[item.replace(/\.js$/, "")] = path.resolve(__dirname, "../src/entry/" + item)
})

var config = {
    //入口文件的配置项
    entry: Object.assign(pageEntry, {
        // main:  path.resolve(__dirname, "../src/main.js")
    }),
    output: {
        //打包的路径
        path: path.resolve(__dirname, buildDir),
        //打包的文件名称
        filename: 'entry/[name].js',
    },
    //模块：例如解读css，图片如何转换，压缩
    module: {
        rules: [
            //css loader
            {
                test: /\.css$/,
                use: extractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "postcss-loader",
                    }, ]
                }),
            },
            {
                test: /\.less$/,
                use: extractTextPlugin.extract({
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "postcss-loader",
                    }, {
                        loader: "less-loader"
                    }],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            },
            //图片 loader
            {
                test: /\.(png|jpg|gif|jpeg)/, //是匹配图片文件后缀名称
                use: [{
                    loader: 'url-loader', //是指定使用的loader和loader的配置参数
                    options: {
                        limit: 500, //是把小于500B的文件打成Base64的格式，写入JS
                        outputPath: 'images/', //打包后的图片放到images文件夹下
                    }
                }]
            }, {
                // test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/,
                test: /\.svg$/,
                loader: 'svg-url-loader?limit=80&name=svg/[hash].[ext]'
            }, {
                test: /\.(woff|eot|ttf|OTF)\??.*$/,
                loader: 'url-loader?limit=80&name=css/[name].[ext]'
            },
            {
                test: /\.(htm|html)$/i,
                use: ['html-withimg-loader'] //解决的问题就是在hmtl文件中引入<img>标签的问题
            },
        ]
    },
    //插件，用于生产模板的各项功能
    plugins: [
        new uglify(),
        new extractTextPlugin("css/[name].css"),
        // new PurifyCSSPlugin({
        //     //这里配置了一个paths，主要是需找html模板，purifycss根据这个配置会遍历你的文件，查找哪些css被使用了。
        //     paths: glob.sync(path.join(__dirname, 'src/*.html')),
        // }),
        new CleanWebpackPlugin(['dist'], {
            root: path.resolve(__dirname, '../'), //根目录 
            verbose: true, //开启在控制台输出信息 
            dry: false //启用删除文件 
        }),
        new CopyWebpackPlugin([{
            from: path.resolve(__dirname, '../src/js'),
            to: path.resolve(__dirname, '../dist/js')
        }])
    ],
    //配置webpack开发服务功能
    devServer: {
        contentBase: path.resolve(__dirname, buildDir),
        host: 'localhost',
        compress: true,
        port: 8888,
        // 设置自动拉起浏览器
        open: true,
        // 设置热更新
        hot: true

    }
}

module.exports = config;

// 输出页面模板
var pageDir = fs.readdirSync(path.resolve(__dirname, "../src"));
name = "";
//得到页面
pageDir.forEach((item, index) => {
    if (path.extname(item) == '.html') {
        name = item.replace(/\.html$/, "");
        config.plugins.push(new htmlPlugin({
            entryName: name,
            filename: item,
            template: path.resolve(__dirname, "../src/" + item),
            inject: false,
            minify: {
                removeComments: false,
                removeAttributeQuotes: false, // 不移除属性的引号
                collapseWhitespace: false
            },
            // chunks: ['main', 'common', name]
        }))
    }
})