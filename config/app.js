//app.js
const express = require("express"),
    path = require('path'),
    webpack = require("webpack"),
    webpackDevMiddleware = require("webpack-dev-middleware"),
    webpackHotMiddleware = require("webpack-hot-middleware"),
    webpackConfig = require("./webpack.dev");
const app = express();
const dll = webpack(webpackConfig);
const compiler = webpack(webpackConfig);

app.use(webpackDevMiddleware(compiler, {
    // publicPath与webpack.config.js保持一致
    publicPath: webpackConfig.output.publicPath,
    noInfo: false,
    stats: {
        colors: true
    }
}));
app.use(webpackHotMiddleware(compiler));
app.use("/",function(req,res){
  res.render("index");
});
const reload = require('reload');
const http = require('http');
const server = http.createServer(app);
reload(app);
server.listen(8888, function(){
    console.log("The Server Connect,The port is "+8888);
}); 