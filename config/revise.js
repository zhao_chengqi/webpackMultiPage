const fs = require('fs');
const path = require("path");


var entryDir = fs.readdirSync(path.resolve(__dirname, "../dist/css"));
let file = "";
//得到入口文件
fs.readdirSync(path.resolve(__dirname, "../dist/css")).forEach((item, index) => {
    let _path = path.resolve(__dirname, "../dist/css/" + item);
    file = fs.readFileSync(_path, 'utf8');
    file = file.replace(/url\(\.\//g, 'url(../');
    //异步方法
    fs.writeFile(_path, file, function (err) {
        if (err) console.log('文件操作失败');
        else console.log('操作成功');
    });
})


// fs.readdirSync(path.resolve(__dirname, "../dist/entry")).forEach((item, index) => {
//     let _path = path.resolve(__dirname, "../dist/entry/" + item);
//     file = fs.readFileSync(_path, 'utf8');
//     file = file.replace(/\".\/\"/g, '"../"');
//     //异步方法
//     fs.writeFile(_path, file, function (err) {
//         if (err) console.log('entry操作失败');
//         else console.log('entry操作成功');
//     });
// })